import requests


def get_location_photo(city, state):
    headers = {
        "Authorization": "D0ulhj04vF3rdmenzI7yFcRlQG9ue2SGZTtmFaRLuinbkooUfQsElF46"
    }

    url = f"https://api.pexels.com/v1/search?query={city}+{state}"

    resp = requests.get(url, headers=headers)
    print(resp)
    return resp.json()["photos"][0]["url"]
