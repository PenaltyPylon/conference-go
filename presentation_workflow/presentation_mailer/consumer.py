import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
import time
from pika.exceptions import AMQPConnectionError

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


while True:
    try:
        def process_approval(ch, method, properties, body):
            result = json.loads(body)
            name = result["presenter_name"]
            title = result["title"]
            email = result["presenter_email"]
            send_mail(
                'Your presentation has been accepted!',
                f"{name}, we're happy to tell you that your presentation {title} has been accepted",
                'admin@conference.go',
                [email],
                fail_silently=False,
                )



        def process_rejection(ch, method, properties, body):
            print("processing rejection")
            result = json.loads(body)
            name = result["presenter_name"]
            title = result["title"]
            email = result["presenter_email"]
            send_mail(
                'Your presentation has been rejected.',
                f"{name}, your presentation {title} has been rejected",
                'admin@conference.go',
                [email],
                fail_silently=False,
                )


        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue='presentation_approvals')
        channel.queue_declare(queue='presentation_rejections')

        channel.basic_consume(
            queue='presentation_rejections',
            on_message_callback=process_rejection,
                auto_ack=True,
            )

        channel.basic_consume(
            queue='presentation_approvals',
            on_message_callback=process_approval,
                auto_ack=True,
            )




        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect RabbitMQ")
        time.sleep(2.0)


# def process_approval(ch, method, properties, body):
#     send_mail(
#         'Your presentation has been accepted!',
#         "{name}, we're happy to tell you that your presentation {title} has been accepted",
#         'from@admin@conference.go',
#         ['presenter_email'],
#         fail_silently=False,
#         ),
#         auto_ack=True,
#     )



# parameters = pika.ConnectionParameters(host='rabbitmq')
# connection = pika.BlockingConnection(parameters)
# channel = connection.channel()
# channel.queue_declare(queue='presentation_approvals')
# channel.basic_consume(
#     queue='presentation_approvals',
#     on_message_callback=send_mail(
#         'Your presentation has been accepted!',
#         "{name}, we're happy to tell you that your presentation {title} has been accepted",
#         'from@admin@conference.go',
#         ['presenter_email'],
#         fail_silently=False,
#         ),
#         auto_ack=True,
#     ),
# channel.start_consuming()
