from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Conference, Location, State
from django.views.decorators.http import require_http_methods
import json
from .acl import get_location_photo


class LocationListEncoder(ModelEncoder):  # a class that takes python datatypes and converts it to json, inheriting from ModelEncoder
    model = Location   # setting the model it uses as Location
    properties = ["name"]  # determining what properties it wants to take from the model

@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )

    else:
        content = json.loads(request.body)

        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=404,
            )

        content["location_url"] = get_location_photo(content["city"], content["state"].name)
        
        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )





# @require_http_methods(["GET", "POST"]) # a decorator that will only allow
# #the listed http methods to be used with this method
# def api_list_locations(request): # a function which takes a request
#     # catch errors and return appropriate status code

#     if request.method == "GET": # if the request method is GET, we'll return/list all the objects
#         locations = Location.objects.all() # get all instances of the object model location and put it in variable 'locations'
#         return JsonResponse(            # jsonresponse is prebuilt method that we import that is the actual response?
#             {"locations": locations},   # returns a dictionary where the key is this--but why this? the key
#         # just needs to be something that makes sense, but it does show up on the insomnia side
#         # the value it's pulling is the one we've assigned to it from the variable
#             encoder=LocationListEncoder, # uses the LocationListEncoder to know to pull name? but it loads both href and name?
#         # does it know to pull the href based on what we've added to the modelencoder?
#         # it knows to pull the href based on the model itself--even if we haven't referenced the href any other place,
#         # if it's on the model then it pulls it automatically
#         )
#         # POST CREATES; the content of the POST is a JSON formatted string stored in request.body
#     else:
#         content = json.loads(request.body) # json.loads converts the json string stored in request.body (converts it
#         # to python dict???). this
#         # information is then stored in the variable content
#         # get the state object (foreign key on location?) and put it in the content dict
#         try:
#             state = State.objects.get(abbreviation=content["state"])
#             content["state"] = state  #this feels redundant--ask about it later
#               # create a new instance from the Location object,
#         # unpack dictionary that has been stored in content??? unpack it to what and where?
#             return JsonResponse(  # return everything back to json?
#                 location,       # return values that have been stored in the location variable
#                 encoder=LocationDetailEncoder,  # use the location detail encoder--why this one and not the
#             # location list encoder? loads all of the properties found on that encoder
#                 safe=False,
#             )
#         except State.DoesNotExist:
#             return JsonResponse(
#             {"message": "Invalid state abbreviation"},
#             status=400,
#             )

#         location = Location.objects.create(**content)
#         return JsonResponse(
#             location
#         )
#         content["location_url"] = ""


# def api_list_locations(request):
#     response = []
#     locations = Location.objects.all()
#     for location in locations:
#         response.append ({
#             "name": location.name,
#             "href": location.get_api_url(),
#         }
#         )
#     return JsonResponse({"locations": response})


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]

@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)
        # get the Location object and put it in the content dict
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """



class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [          # take all of these properties from the model to display in json
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(), # this attribute/propery is a foreignkey to the model Location
        # with a related name "conferences", so it's pulling the information we want
        # from the locationlistencoder? the information is...href and name
     }

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):
    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder, safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        #try:
        # # convert the state abbreviation into a state, if it exists
        #     if "state" in content:
        #         state = State.objects.get(abbreviation=content["state"])
        #         content["state"] = state
        # except State.DoesNotExist:
        #     return JsonResponse(
        #         {"message": "Invalid state abbreviation"},
        #         status=400,
         #   )
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(  # return everything back to json?
                conference,       # return values that have been stored in the location variable
                encoder=ConferenceDetailEncoder,  # use the location detail encoder--why this one and not the
            # location list encoder? loads all of the properties found on that encoder
                safe=False,
            )


    """
    Returns the details for the Conference model specified
    by the id parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """





    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """

class LocationDetailEncoder(ModelEncoder):
    model = Location   # get attributes/properties from this model
    properties = [
        "name",
        "city",
        "location_url",
        "room_count",
        "created",
        "updated",
    ]

def get_extra_data(self, o):  # o is shorthand for every possible attribute?
    return { "state": o.state.abbreviation } # state is a foreignkey to the model state
    # if we had a StateEncoder, we'd probably reference this the way we did for our
    # location encoder

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder, # why do we include encoders in the json response for our detail stuff?
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete() # filter the object by the id and delete it,
        # don't understand count, _
        return JsonResponse({"deleted": count > 0})
    else:
        # convert the submitted json string into a dictionary

        content = json.loads(request.body)
        try:
        # convert the state abbreviation into a state, if it exists
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        return JsonResponse(  # return everything back to json?
                location,       # return values that have been stored in the location variable
                encoder=LocationDetailEncoder,  # use the location detail encoder--why this one and not the
            # location list encoder? loads all of the properties found on that encoder
                safe=False,
            )

# def api_show_location(request, id):
#     location = Location.objects.get(id=id)
#     return JsonResponse(
#         {
#             "name": location.name,
#             "city": location.city,
#             "room_count": location.room_count,
#             "created": location.created,
#             "updated": location.updated,
#             "state": {
#                 "state_abbreviation": location.state.abbreviation,
#             },
#         }
#     )
    """
    Returns the details for the Location model specified
    by the id parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """
